[Long Vectors](https://stat.ethz.ch/R-manual/R-devel/library/base/html/LongVectors.html) benchmark.  
Ultimately this project is going to be merged into [db-benchmark](https://github.com/h2oai/db-benchmark).  

# Tasks

  - [ ] groupby
  - [ ] join

# Solutions

  - [ ] [data.table](https://github.com/Rdatatable/data.table)
  - [ ] [dplyr](https://github.com/tidyverse/dplyr)
  - [ ] [base R](https://www.r-project.org)

As of now only R solutions are in scope of this project. Once it will be mature enough to be merged into [db-benchmark](https://github.com/h2oai/db-benchmark) other solutions will be added.

# Reproduce

  - generate data using `Rscript groupby-datagen.R 3e9 1e8 0 0`, move `rds` files to `data` directory
  - edit `run.sh` to list your data in `SRC_GRP_LOCAL` env vars
  - `./run.sh`
