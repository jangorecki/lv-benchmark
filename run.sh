#!/bin/bash
set -e

## run script the following way to exit if benchmark is already running
#if [[ -f ./run.lock ]]; then echo "# Benchmark run discarded due to previous run $(cat run.lock) still running" > "./run_discarded_at_$(date +%s).out"; else ./run.sh > ./run.out; fi;

# set batch
export BATCH=$(date +%s)

# confirm stop flag disabled
if [[ -f ./stop ]]; then echo "# Benchmark run $BATCH aborted. 'stop' file exists, should be removed before calling 'run.sh'" && exit; fi;

# confirm swap disabled
Rscript -e 'swap_all<-data.table::fread("free -h | grep Swap", header=FALSE)[, -1L][, as.numeric(gsub("[^0-9.]", "", unlist(.SD)))]; swap_off<-!is.na(s<-sum(swap_all)) && s==0; q("no", status=as.numeric(swap_off))' && echo "# Benchmark run $BATCH aborted. swap is enabled, 'free -h' has to report only 0s for Swap, run 'swapoff -a' before calling 'run.sh'" && exit;

# ensure directories exists
mkdir -p ./out
if [[ ! -d ./data ]]; then echo "# Benchmark run $BATCH aborted. './data' directory does not exists" && exit; fi;

# set lock
if [[ -f ./run.lock ]]; then echo "# Benchmark run $BATCH aborted. 'run.lock' file exists, this should be checked before calling 'run.sh'. Ouput redirection mismatch might have happened if writing output to same file as currently running $(cat ./run.lock) benchmark run" && exit; else echo $BATCH > run.lock; fi;

echo "# Benchmark run $BATCH started"

export SRC_GRP_LOCAL=LV_1e6_1e2_0_0
Rscript ./groupby-baser.R > ./out/run_baser_groupby2_$SRC_GRP_LOCAL.out 2> ./out/run_baser_groupby2_$SRC_GRP_LOCAL.err
Rscript ./groupby-datatable.R > ./out/run_datatable_groupby2_$SRC_GRP_LOCAL.out 2> ./out/run_datatable_groupby2_$SRC_GRP_LOCAL.err
export SRC_GRP_LOCAL=LV_1e6_1e3_0_0
Rscript ./groupby-baser.R > ./out/run_baser_groupby2_$SRC_GRP_LOCAL.out 2> ./out/run_baser_groupby2_$SRC_GRP_LOCAL.err
Rscript ./groupby-datatable.R > ./out/run_datatable_groupby2_$SRC_GRP_LOCAL.out 2> ./out/run_datatable_groupby2_$SRC_GRP_LOCAL.err

# remove run lock file
rm -f ./run.lock

# completed
echo "# Benchmark run $BATCH has been completed in $(($(date +%s)-$BATCH))s"
